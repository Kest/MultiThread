import org.junit.Test;
import second.ReaderWriter;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class TestMultiThread {

    @Test
    public void showTimeTrue() throws IOException, ExecutionException, InterruptedException {
        long timer = 0;
        for (int i = 0; i < 100; i++) {
            ReaderWriter readerWriter = new ReaderWriter("/Users/macbook/IdeaProjects/MultiThread/src/main/resources/start.txt",
                    "/Users/macbook/IdeaProjects/MultiThread/src/main/resources/result.txt", 15, true, "Д.*");
            long t1 = System.nanoTime();
            readerWriter.filter();
            long t2 = System.nanoTime();
            readerWriter.close();
            timer += (t2 - t1);
        }
        System.out.println(timer/100);
    }

    @Test
    public void showTimeFalse() throws IOException, ExecutionException, InterruptedException {
        long timer = 0;
        for (int i = 0; i < 100; i++) {
            ReaderWriter readerWriter = new ReaderWriter("/Users/macbook/IdeaProjects/MultiThread/src/main/resources/start.txt",
                    "/Users/macbook/IdeaProjects/MultiThread/src/main/resources/result.txt", 15, false, "Д.*");
            long t1 = System.nanoTime();
            readerWriter.filter();
            long t2 = System.nanoTime();
            readerWriter.close();
            timer += (t2 - t1);
        }
        System.out.println(timer/100);
    }
}
