package first;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Downloader{
    ExecutorService service;

    final int poolSize;

    public Downloader(int poolSize) {
        this.poolSize = poolSize;
        service = Executors.newFixedThreadPool(poolSize);
    }

    public List<String> downloadContent(List<String> urls) {
        ArrayList<String> contents = new ArrayList<>();
        ArrayList<FutureTask<String>> tasks = new ArrayList<>();
        try {
            for (int i = 0; i < urls.size();) {
                for (int j = 0; j < poolSize; j++) {
                    if (i > 14)
                        break;
                    tasks.add(new FutureTask<>(new DownloadContent(urls.get(i))));
                    service.submit(tasks.get(j));
                    i++;
                }
                for (int j = 0; j < tasks.size(); j++) {
                    contents.add(tasks.get(j).get());
                }
                tasks.clear();
            }
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return contents;
    }

    public void close() {
        service.shutdown();
    }

    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();
        list.add("https://mail.ru");
        list.add("https://habr.com/company/luxoft/blog/157273/");
        list.add("http://java-online.ru/concurrent-executor.xhtml");
        list.add("https://javadevblog.com/java-callable-kratkoe-opisanie-i-primer-ispol-zovaniya.html");
        list.add("https://yandex.ru/maps/2/saint-petersburg/?ll=30.315868%2C59.939095&z=11");
        list.add("https://mail.ru");
        list.add("https://habr.com/company/luxoft/blog/157273/");
        list.add("http://java-online.ru/concurrent-executor.xhtml");
        list.add("https://javadevblog.com/java-callable-kratkoe-opisanie-i-primer-ispol-zovaniya.html");
        list.add("https://yandex.ru/maps/2/saint-petersburg/?ll=30.315868%2C59.939095&z=11");
        list.add("https://mail.ru");
        list.add("https://habr.com/company/luxoft/blog/157273/");
        list.add("http://java-online.ru/concurrent-executor.xhtml");
        list.add("https://javadevblog.com/java-callable-kratkoe-opisanie-i-primer-ispol-zovaniya.html");
        list.add("https://yandex.ru/maps/2/saint-petersburg/?ll=30.315868%2C59.939095&z=11");

        Downloader downloader = new Downloader(4);
        System.out.println(downloader.downloadContent(list));
        downloader.close();

    }
}
