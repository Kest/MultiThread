package second;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

public class Writer implements Runnable {

    private FileWriter writer;

    ArrayBlockingQueue<String> queueForWrite;

    public Writer(String path, ArrayBlockingQueue<String> queueForWrite) throws IOException {
        writer = new FileWriter(path);
        this.queueForWrite = queueForWrite;
    }

    @Override
    public void run() {
        boolean b = true;
        while (b){
            if (queueForWrite.peek() != null) {
                if (queueForWrite.peek().equals("@end")) {
                    break;
                }
                try {
                    writer.write(queueForWrite.poll() + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
