package second;

public class Flag {
    private boolean flag = true;

    public Flag() {
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
